$logDirectory = "C:\temp"

$logFiles = Get-ChildItem -Path $logDirectory -Filter "*.log"

Stop-Service -Name "AmazonCloudWatchAgent"

foreach ($file in $logFiles) {
    try {
    
        $fileStream = [System.IO.File]::Open($file.FullName, 'Open', 'ReadWrite', 'None')
        $streamReader = New-Object System.IO.StreamReader($fileStream)
        $streamReader.ReadToEnd()
        $streamReader.Close()
        $fileStream.Close()

        $rotatedFiles = Get-ChildItem -Path $logDirectory -Filter "$($file.BaseName)_*" | Where-Object { $_.Extension -notlike ".log" } | Sort-Object LastWriteTime -Descending

        if ($rotatedFiles.Count -ge 3) {
            $rotatedFiles[-1] | Remove-Item -Force
        }

        $timestamp = Get-Date -Format "yyyyMMddHHmmss"
        $newName = "{0}_{1}" -f $file.BaseName, $timestamp
        $newPath = Join-Path -Path $logDirectory -ChildPath $newName
        Get-Content -Path $file.FullName | Set-Content -Path $newPath -Force

        [System.IO.File]::WriteAllText($file.FullName, "")

    } catch {
        Write-Host "Error accessing $($file.Name): $_"
    }
}

Start-Service -Name "AmazonCloudWatchAgent"
