module "ssm_custom_monitors" {

  source                                                                  = "./module-ssm_custom_monitors.tf"
}

provider "aws" {
}


terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }

  }

  required_version = ">= 1.0"
}

