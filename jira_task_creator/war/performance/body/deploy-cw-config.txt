*Acceptance Criteria*
* Add EC2 SSM managed policy if required
* Work within customer maintenance window if required
* Deploy the created configurations to instances to be monitored
* Validate expected metrics and logs in CloudWatch
* Alarm if necessary in followup tasks