# Terraform Settings Block
terraform {
  required_version = ">= 1.0.0"
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 5.36"
     }
  }
  backend "s3" {
    bucket = "terraform-on-aws-eks-n8"
    key    = "eks-cluster/terraform.tfstate"
    region = "ca-central-1"

    # For State Locking
    dynamodb_table = "eks-cluster"
  }
}



# Terraform Provider Block
provider "aws" {
  region = "ca-central-1"
}