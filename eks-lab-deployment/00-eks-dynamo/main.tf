module "dynamodb_table" {
  for_each = toset(var.tables)

  source   = "terraform-aws-modules/dynamodb-table/aws"
  name     = each.value
  hash_key = "LockID"

  attributes = [
    {
      name = "LockID"
      type = "S"
    }
  ]

  tags = {
    Terraform   = "true"
    Environment = "${terraform.workspace}"
  }
}