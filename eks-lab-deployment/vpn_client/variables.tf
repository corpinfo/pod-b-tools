variable "server_certificate_arn" {
    type = string
}
variable "vpn_client_root_arn" {
    type    = string
    default = null
}
variable "vpn_client_directory_id" {
    type    = string
    default = null
}
variable "client_vpn_subnet" {
    type = list(string)
}
variable "client_vpn_sg" {
    type = string
}
variable "vpc_id" {
    type = string
}
variable "description" {
    type    = string
    default = "client"
}
variable "cidr_block" {
    type    = string
    default = "10.20.0.0/22"
}