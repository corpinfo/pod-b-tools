# s3 Bucket for replication target
resource "aws_s3_bucket" "snowflake-replication-bucket" {
  # aws_s3_bucket_lifecycle_configuration
  bucket = "snowflake-replication-cdc-bucket-test"
  force_destroy = true
  # bucket_prefix = "snowflake-cdc-bucket"
  tags = {
    "Name" = "snowflake-replication-cdc-bucket-test"
    "Environment" = "Dev"
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "cdc-lifecycle-config" {
  bucket = aws_s3_bucket.snowflake-replication-bucket.id
  rule {
    id = "remove-old-files"
    status = "Enabled"
    filter {
      # # Object size in bytes
      # object_size_greater_than = 500
      # prefix = "test/"
      # tag {
      #   key = "Environment"
      #   value = "Dev"
      # }
    }
    expiration {
      days = 7
    }
    # # Multiple filters
    # filter {
    #   and {
    #     prefix = "test/"
    #     tags = {
    #       Key1 = "Value1"
    #       Key2 = "Value2"
    #     }
    #     object_size_greater_than = 500
    #     object_size_less_than    = 64000
    #   }
    # }
  }
#  rule {
#     id = "remove-old-files-2"
#     status = "Enabled"
#     filter {
#       # prefix = "test/"
#     }
#     expiration {
#       days = 7
#     }
#   }
}
