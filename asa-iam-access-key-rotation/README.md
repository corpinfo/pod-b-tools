# asa-iam-access-key-lambda

This is a serverless deployment of an IAM Key Rotation Solution that is provided by AWS.

### Deployment Steps

To kickoff this deployment, you must first obtain AWS access key credentials to the master payer account (or just your lab account) using Rackspace's login portal (https://manage.rackspace.com/racker).

From the project root directory, run `make deploy-common` to run this deployment.

TODO: Add a custom hook to upload the html file to awslambda/functions/function_email_template/ directory.  Process is currently manual after initial deployment is run.

#### Configured deployments

1: Master Pay Deployment / Main Rotation Solution

* Main Rotation and Notifier (us-west-2)
* List Accounts Role (us-west-2)

2: Dev-Account-Deployment

- IAM-Key-Rotation-Assumed-Roles (us-west-2)

3: QA-Account-Deployment

- IAM-Key-Rotation-Assumed-Roles (us-west-2)

4: Prod-Account-Deployment

- IAM-Key-Rotation-Assumed-Roles (us-west-2)

5: Ops-Account-Deployment

- IAM-Key-Rotation-Assumed-Roles (us-west-2)
