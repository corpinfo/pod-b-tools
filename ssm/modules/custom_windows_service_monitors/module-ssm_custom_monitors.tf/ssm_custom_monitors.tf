

data "local_file" "check_service_script" {
  filename = "${path.module}/scripts/check_windows_services.ps1"
}

data "local_file" "cloudwatch_config_windows" {
  filename = "${path.module}/scripts/cw_agent_config_for_services.json"
}

data "local_file" "service_association_parameters" {
  filename = "${path.module}/scripts/parameters_for_service.ps1"
}

data "local_file" "windows_log_rotation" {
  filename = "${path.module}/scripts/log_rotate.ps1"
}  

data "local_file" "log_rotation_parameters" {
  filename = "${path.module}/scripts/parameters_for_log_rotation.ps1"
}


resource "aws_ssm_parameter" "windows_log_rotation_parameters" {
  name = "windows_log_rotation_parameters"
  type = "String"
  value = data.local_file.log_rotation_parameters.content
}

resource "aws_ssm_parameter" "windows_log_rotation_script" {
  name  = "windows_log_rotation"
  type  = "String"
  value = data.local_file.windows_log_rotation.content
}

resource "aws_ssm_parameter" "windows_service_script" {
  name  = "check_windows_services"
  type  = "String"
  value = data.local_file.check_service_script.content
}


resource "aws_ssm_parameter" "cw_agent_config_windows" {
  name  = "windows_cw_agent_config"
  type  = "String"
  value = data.local_file.cloudwatch_config_windows.content
}



resource "aws_ssm_association" "windows_monitor_script" {
  name                = "AWS-RunPowerShellScript"
  association_name    = "Windows-Monitor-Script-TF"
  targets {
    key    = "InstanceIds"
    values = [var.target_instance]
   }

  parameters = {
    commands = data.local_file.service_association_parameters.content
 
 }

}


resource "aws_ssm_association" "ssm-cloudwatch-agent" {
  name             = "AWS-ConfigureAWSPackage"
  association_name = "Install-CW-Agent-TF"
  parameters = {
    action = "Install"
    name   = "AmazonCloudWatchAgent"
    }
  targets {
    key    = "InstanceIds"
    values = [var.target_instance]
  }
}
 
 
 resource "aws_ssm_association" "configure-cloudwatch-agent" {
   name             = "AmazonCloudWatch-ManageAgent"
   association_name = "Configure-CW-Agent-TF"
   parameters = {
     action = "configure"
     mode   = "ec2"
     optionalRestart  = "yes"
     optionalConfigurationSource = "ssm"
     optionalConfigurationLocation = aws_ssm_parameter.cw_agent_config_windows.name
    }
  targets {
    key    = "InstanceIds"
    values = [var.target_instance]
 }
}

 resource "aws_ssm_association" "install-log-rotate-win" {
   name             = "AWS-RunPowerShellScript"
   association_name = "Log-Rotation-Script-TF"
    targets {
      key    = "InstanceIds"
      values = [var.target_instance]
  }

  parameters = {
    commands = data.local_file.log_rotation_parameters.content
 
  }

}
   
   
output "parameter_arn" {
  value = [aws_ssm_parameter.windows_service_script.arn, aws_ssm_parameter.cw_agent_config_windows.arn]

}
