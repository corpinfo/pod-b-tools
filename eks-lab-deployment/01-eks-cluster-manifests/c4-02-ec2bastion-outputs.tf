# AWS EC2 Instance Terraform Outputs
# Public EC2 Instances - Bastion Host

## ec2_bastion_public_instance_ids
output "ec2_bastion_public_instance_ids" {
  description = "List of IDs of instances"
  value       = length(module.ec2_public) > 0 ? [module.ec2_public[0].id] : ["Bastion not enabled"]
}

## ec2_bastion_public_ip
output "ec2_bastion_eip" {
  description = "Elastic IP associated with the Bastion Host"
  value = length(aws_eip.bastion_eip) > 0 ? aws_eip.bastion_eip[0].public_ip : "EIP not created"
}

