#!/bin/bash

# Check if a file is provided as an argument
if [ -z "$1" ]; then
    echo "Usage: $0 <input.yaml>"
    exit 1
fi

# Check if the input file exists
if [ ! -f "$1" ]; then
    echo "File $1 not found!"
    exit 1
fi

# Extract the filename without extension to use for the output JSON file
filename=$(basename -- "$1")
filename_noext="${filename%.*}"

# Convert YAML to JSON using Python
python -c 'import sys, yaml, json; json.dump(yaml.load(sys.stdin, Loader=yaml.FullLoader), sys.stdout)' < "$1" > "${filename_noext}.json"

echo "Converted $1 to ${filename_noext}.json"
