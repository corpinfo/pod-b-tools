#!/bin/bash

while getopts r:e:i: input
 do 
   case "${input}" in
        r) region=${OPTARG};;
        i) IFS=' ' 
           include=("${include[@]}" $OPTARG);;
        \?)
           echo "Invalid option: -$OPTARG" >&2
           exit 1
           ;;
        :)
           echo "Option -$OPTARG requires an argument." >&2
           exit 1
           ;;
    esac
 done

 
if [ -z $region ]; then
  echo "Option -r is required and must have a value for the region." >&2;
  exit 1
  else
  


if [ ${#include[@]} -gt 0 ]; then
  for i in "${include[@]}"; do
    echo "Generating output for the $i service in the region $region."
    former2 generate --output-terraform "$i-$region.hcl" --region "$region" --services "$i"
  done
else
  echo "Option -i was not provided, generating all services in region $region. "
  services=(1Click APIGateway AmazonMQ Amplify Amplify Analytics AppConfig AppConfig AppFlow AppMesh AppStream AppSync Athena AuditManager AutoScaling Backup Batch Budgets CertificateManager Cloud9 CloudFront CloudHSM CloudMap CloudTrail CloudWatch CodeArtifact CodeArtifact CodeBuild CodeCommit CodeDeploy CodeGuru CodePipeline CodeStar Cognito Config Core CostExplorer DataBrew DataPipeline DataSync DatabaseMigrationService Detective DevOpsGuru DeviceFarm DirectConnect DirectoryService DocumentDB DynamoDB EC2 EC2ImageBuilder ECR ECS EFS EKS EMR ElastiCache ElasticBeanstalk ElasticTranscoder Elasticsearch EventBridge Events FSx GameLift Glacier GlobalAccelerator Glue Greengrass GroundStation GuardDuty IAM Inspector InteractiveVideoService KMS Kendra Kinesis LakeFormation Lambda Lex LicenseManager Lightsail LookoutForVision MSK Macie ManagedApacheAirflow ManagedBlockchain ManagedBlockchain MediaConnect MediaConvert MediaLive MediaPackage MediaStore Neptune OpsWorks Organizations Pinpoint QLDB QuickSight RDS Redshift ResourceAccessManager ResourceGroups RoboMaker Route53 S3 SES SNS SQS SWF SageMaker SecretsManager SecurityHub ServiceCatalog ServiceQuotas Signer SimpleDB SingleSignOn SiteWise StepFunctions StorageGateway SystemsManager ThingsGraph Timestream Transfer VPC WAFAndShield WorkLink WorkSpaces XRay)
  for i in "${services[@]}"
   do 
    
     echo "Generating output for the $i service in the region $region."
     former2 generate --output-terraform "$i-$region.hcl" --region "$region" --services "$i"
   done

 fi
 fi
