#!/bin/bash

timestamp=$(date +"%Y%m%d%H%M%S")

usage () {
    echo "Usage: $0 -a ACCOUNT_ID"
    echo "Options:"
    echo " -a ACCOUNT_ID The AWS account ID you need the QuickSight data for."
    echo " -h help"
    echo " -e Everything"
    echo " -s Data Sources"
    echo " -d Data Sets"
    echo " -l Analyses"
    echo " -b Dashboards"
    echo " -u Users"
    echo " Ensure you are logged into the account with the correct region for Quicksight, and have the correct Quicksight permissions."
    exit 1
}


datasources(){

DS_FILENAME="quicksight-data-source-list-$ACCOUNT_ID-$timestamp.json"
DS_DESC_FILE="quicksight-data-source-descriptions-$ACCOUNT_ID-$timestamp.json"
DS_PERM_FILE="quicksight-data-source-permissions-$ACCOUNT_ID-$timestamp.json"

echo "Grabbing the list of data sources..."
aws quicksight list-data-sources --aws-account-id $ACCOUNT_ID > $DS_FILENAME


echo "Fetching the data source descriptions..."
for i in $(grep DataSourceId $DS_FILENAME | awk -F ':' '{print $2}' |  tr -d '" ,'); do aws quicksight describe-data-source --data-source-id $i --aws-account-id $ACCOUNT_ID >> $DS_DESC_FILE ; done


echo "Fetching the data source permissions..."
for i in $(grep DataSourceId $DS_FILENAME | awk -F ':' '{print $2}' |  tr -d '" ,'); do aws quicksight describe-data-source-permissions --data-source-id $i --aws-account-id $ACCOUNT_ID >> $DS_PERM_FILE ; done

echo -e "Done with data sources. Wrote: \\n   $DS_FILENAME \n   $DS_DESC_FILE \n   $DS_PERM_FILE"
}


datasets(){

SETS_FILENAME="quicksight-data-set-list-$ACCOUNT_ID-$timestamp.json"
SET_DESC_FILE="quicksight-data-set-description-$ACCOUNT_ID-$timestamp.json"
SET_PERM_FILE="quicksight-data-set-permissions-$ACCOUNT_ID-$timestamp.json"

echo -e "\nGrabbing the list of datasets..."
aws quicksight list-data-sets --aws-account-id $ACCOUNT_ID > $SETS_FILENAME

echo "Fetching the dataset descriptions..."
for i in $(grep DataSetId $SETS_FILENAME | awk -F ':' '{print $2}' |  tr -d '" ,'); do aws quicksight describe-data-set --data-set-id $i --aws-account-id $ACCOUNT_ID > $SET_DESC_FILE ; done

echo "Fetching dataset permissions..."
for i in $(grep DataSetId $SETS_FILENAME | awk -F ':' '{print $2}' |  tr -d '" ,'); do aws quicksight describe-data-set-permissions --data-set-id $i --aws-account-id $ACCOUNT_ID >> $SET_PERM_FILE ; done

echo -e "\nDone with datasets. Wrote: \n   $SETS_FILENAME \n   $SET_DESC_FILE \n   $SET_PERM_FILE"
}


analyses(){
A_LIST="quicksight-analyses-list-$ACCOUNT_ID-$timestamp.json"
A_DESC="quicksight-analyses-descriptions-$ACCOUNT_ID-$timestamp.json"
A_PERM="quicksight-analyses-permissions-$ACCOUNT_ID-$timestamp.json"

echo -e "\nGrabbing the list of analyses..."
aws quicksight list-analyses --aws-account-id $ACCOUNT_ID > $A_LIST

echo "Fetching analyses descriptions..."
for i in $(grep AnalysisId $A_LIST | awk -F ':' '{print $2}' |  tr -d '" ,'); do aws quicksight describe-analysis --analysis-id $i --aws-account-id $ACCOUNT_ID >> $A_DESC ; done

echo -e "\nDone with analyses. Wrote: \n   $A_LIST \n   $A_DESC"

echo "Fetching analyses permissions..."
for i in $(grep AnalysisId $A_LIST | awk -F ':' '{print $2}' |  tr -d '" ,'); do aws quicksight describe-analysis-permissions --analysis-id $i --aws-account-id $ACCOUNT_ID >> $A_PERM ; done

echo -e "\nDone with analyses. Wrote: \n   $A_LIST \n   $A_DESC \n   $A_PERM" 
}

dashboards(){
DASH_LIST="quicksight-dashboards-$ACCOUNT_ID-$timestamp.json"
DASH_DESC="quicksight-dashboard-descriptions-$ACCOUNT_ID-$timestamp.json"
DASH_PERM="quicksight-dashboard-permissions-$ACCOUNT_ID-$timestamp.json"

echo -e "\nGrabbing the list of dashboards..."
aws quicksight list-dashboards --aws-account-id $ACCOUNT_ID > $DASH_LIST

echo -e "Fetching the dashboard descriptions..."
for i in $(grep DashboardId $DASH_LIST | awk -F ':' '{print $2}' |  tr -d '" ,'); do aws quicksight describe-dashboard --dashboard-id $i --aws-account-id $ACCOUNT_ID >> $DASH_DESC ; done

echo -e "Grabbing dashboard permissions..."
for i in $(grep DashboardId $DASH_LIST | awk -F ':' '{print $2}' |  tr -d '" ,'); do aws quicksight describe-dashboard-permissions --dashboard-id $i --aws-account-id $ACCOUNT_ID >> $DASH_PERM ; done



echo -e "\nDone with dashboards. Wrote: \n   $DASH_LIST \n   $DASH_DESC \n   $DASH_PERM"
}


users(){
USER_DESC="quicksight-users-$ACCOUNT_ID-$timestamp.json"

echo -e "\nGrabbing the list of uers..."
aws quicksight list-users --namespace default --aws-account-id $ACCOUNT_ID > $USER_DESC

echo -e "\nDone with users. Wrote: \n   $USER_DESC"
}


all_functions(){
	datasources
	datasets
	analyses
	dashboards
	users
}



### We'll use the vars for storing the command line flags

datasources_cl=false
datasets_cl=false
analyses_cl=false
dashboards_cl=false
users_cl=false
all_cl=false



while getopts "a:hesdlbu" opt; do
  case $opt in
     a)
      ACCOUNT_ID="$OPTARG"
      ;;
     h)
      usage
      ;;
     e)
      all_cl=true
      ;;
     s)
      datasources_cl=true
      ;;
     d)
      datasets_cl=true
      ;;
     l)
      analyses_cl=true
      ;;
     b)
      dashboards_cl=true
      ;;
     u)
      users_cl=true
      ;;
    \?)
    usage;;
  esac
done

if [ -z "$ACCOUNT_ID" ]; then
  echo "Error - An account ID is required!"
  usage
fi

## Let's make $1 the first non-option arg
shift $((OPTIND -1))


## Let's check if the flags are set and call the functions
if $all_cl; then
	all_functions
fi
  if $datasources_cl; then
	 datasources
  fi
    if $datasets_cl; then
	  datasets
    fi
      if $analsyes_cl; then
	   analyses
      fi
        if $dashboards_cl; then
	    dashboards
        fi
          if $users_cl; then
	     users
          fi	     
