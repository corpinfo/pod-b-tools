#!/bin/zsh

# Check if the ~/.aws/credentials file exists
if [[ -e ~/.aws/credentials ]]; then
  # If it exists, rename it with a timestamp
  timestamp=$(date +%Y%m%d_%H%M%S)
  mv ~/.aws/credentials ~/.aws/credentials.$timestamp
fi

# Set the list of AWS profiles
accounts=$(onica-sso list accounts 2>/dev/null | awk '{print $1}')

# Initialize the variable to hold the list of accounts that pod-b has access to
pod_b_accounts=""

for account in $accounts; do
  account_details=$(onica-sso get account info $account 2>/dev/null )
  admin_tags=$(echo $account_details | awk '/admin:/,/readonly:/{print}')
  if echo $admin_tags | grep -q -E 'tags.*\bpod-b\b'; then
    echo "pod-b has access to $account"
    # Concatenate the account to the list of accounts that pod-b has access to
    pod_b_accounts="$pod_b_accounts $account"
  else
    echo "WARNING: pod-b does not have access to $account"
  fi
done

# Print the list of accounts that pod-b has access to
echo "Accounts that pod-b has access to: $pod_b_accounts"

