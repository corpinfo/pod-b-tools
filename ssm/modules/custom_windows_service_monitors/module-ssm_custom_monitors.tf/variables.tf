variable "target_instance" {
   description = "This will be the target instance for the deployment"
   type        = string
   default     = "i-069717879507e3558"
}

### cron var for potenial future use

variable "cron_schedule" {
   description = "Set the cron schedule here"
   type        = string
   default     = "rate(30 minutes)"
}
