output "dms_replication_subnet_group" {
  value = aws_dms_replication_subnet_group.replication-test.id
}

output "dms_ip" {
  value = aws_dms_replication_instance.replication-test.replication_instance_private_ips
}

output "replication_bucket" {
  value = aws_s3_bucket.snowflake-replication-bucket.id
}

# output "source_endpoint_name" {
#   value = aws_dms_endpoint.dms-endpoint-source.endpoint_id
# }

# output "target_endpoint_name" {
#   value = aws_dms_endpoint.dms-endpoint-target.endpoint_id
# }

# output "dms_replication_task_name" {
#   value = aws_dms_replication_task.postgres-cdc-task.replication_task_id
# }

