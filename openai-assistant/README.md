# OpenAI Assistant Tool

This tool is designed to assist with file conversions and general file help using OpenAI's API.  The data is not used for training and is not stored.

## Requirements

- Python 3.11+
- Poetry package manager
- OpenAI API Token

## Installation

1. **Clone the Repository**

   ```bash
   git clone bibucket/openai-assistant.git
   cd openai-assistant
   ```
2. **Install Dependencies**

   Run the following command to install the required Python packages:

   ```bash
   poetry install
   ```
3. **Activate Virtual Environment**

   After running `poetry install`, a virtual environment should have been created. Activate it by running the appropriate command for your shell:

   - **For Fish Shell**:

     ```fish
     source (poetry env info --path)/bin/activate.fish
     ```
   - **For Bash/Zsh**:

     ```bash
     source $(poetry env info --path)/bin/activate
     ```
4. **Set Up OpenAI API Key**

   You'll need to set your OpenAI API key as an environment variable. Here's how to do it for different shells:

   - **For Fish Shell**:

     ```fish
     set -x OPENAI_API_KEY <your_openai_key_here>
     ```
   - **For Bash/Zsh**:

     ```bash
     export OPENAI_API_KEY=<your_openai_key_here>
     ```

   Replace `<your_openai_key_here>` with your actual OpenAI API key.

## Usage

   To run the assistant tool, execute the following command:

```bash
   assistant <file_path>
```

   Replace `<file_path>` with the path to the file whose content you want to send to OpenAI.

To deactivate the python environment, simply run `deactivate`.

---
