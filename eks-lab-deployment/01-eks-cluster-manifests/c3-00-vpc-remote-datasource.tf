data "terraform_remote_state" "eks-vpc" {
  backend = "s3"
  config = {
    bucket = "terraform-on-aws-eks-n8"
    key    = "env:/${terraform.workspace}/eks-vpc/terraform.tfstate"
    region = var.aws_region
  }
}