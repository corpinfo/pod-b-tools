*Acceptance Criteria*
* Using appropriate IaC, create AWS Config policies that were discussed in the customer review
* Any action on compliance is to occur in followup tasks
* All configuration tested and committed to customer repo