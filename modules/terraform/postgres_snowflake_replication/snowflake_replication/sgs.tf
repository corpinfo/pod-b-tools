# Security Group setup for DMS Instance
resource "aws_security_group" "dms-replication-test-sg" {
  name        = "dms-replication-test-sg"
  description = "SG for DMS snowflake replication instance"
  vpc_id      = var.vpc_id

  tags = {
    Name = "dms-replication-test-sg"
  }

  lifecycle {
    create_before_destroy = true
  }

}

# Security Group setup for Postgres Instance
resource "aws_security_group" "postgres-replication-test-sg" {
  name        = "postgres-replication-test-sg"
  description = "SG for postgres instance"
  vpc_id      = var.vpc_id

  tags = {
    Name = "postgres-replication-test-sg"
  }

  lifecycle {
    create_before_destroy = true
  }

}