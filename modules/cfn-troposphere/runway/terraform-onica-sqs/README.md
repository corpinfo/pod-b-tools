# Terraform AWS Simple Queue Service (SQS) Module

This Terraform module creates an AWS Simple Queue Service (SQS) queue and its corresponding dead-letter queue (DLQ), along with their respective policies and re-drive policies.

## Features

* Creates an SQS queue
* Optionally creates a dead-letter queue (DLQ)
* Configures queue policy and DLQ policy
* Configures re-drive policy and redrive allow policy

## Usage

<pre><div class="bg-black mb-4 rounded-md"><div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans"><span class="">hcl</span><button class="flex ml-auto gap-2"><svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" class="h-4 w-4" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>Copy code</button></div><div class="p-4 overflow-y-auto"><code class="!whitespace-pre hljs language-hcl">module "sqs" {
  source = "<path-to-module>"

  # Main Queue Configuration
  create                       = true
  name                         = "example-queue"
  fifo_queue                   = true
  content_based_deduplication  = true
  max_message_size             = 262144

  # Dead Letter Queue Configuration
  create_dlq                   = true
  dlq_name                     = "example-dlq"

  # Queue Policy Configuration
  create_queue_policy          = true
  create_dlq_queue_policy      = true

  # Additional Configuration
  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}
</code></div></div></pre>

## Input Variables

The following input variables are available to configure the SQS module:

* Main Queue Configuration:
  * `create`: Whether to create the queue (default: true)
  * `name`: Name of the queue
  * `fifo_queue`: Whether to create a FIFO queue (default: false)
  * `content_based_deduplication`: Enables content-based deduplication (default: false)
  * `max_message_size`: Maximum message size (default: 262144)
* Dead Letter Queue Configuration:
  * `create_dlq`: Whether to create a dead-letter queue (default: false)
  * `dlq_name`: Name of the dead-letter queue
* Queue Policy Configuration:
  * `create_queue_policy`: Whether to create a policy for the queue (default: true)
  * `create_dlq_queue_policy`: Whether to create a policy for the dead-letter queue (default: true)
* Additional Configuration:
  * `tags`: A map of tags to add to the queue and dead-letter queue

## Outputs

The following outputs are available from the SQS module:

* `queue_url`: URL of the created SQS queue
* `queue_arn`: ARN of the created SQS queue
* `dlq_url`: URL of the created dead-letter queue (if applicable)
* `dlq_arn`: ARN of the created dead-letter queue (if applicable)

## Requirements

* Terraform version: `>= 1.0`
* AWS Provider version: `>= 4.36`

## License

This module is released under the MIT License. Please see the [LICENSE](https://chat.openai.com/LICENSE) file for more information.
