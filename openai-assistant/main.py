import os
import argparse
import time
import threading
from assistant.chatgptmax import send

def read_file_content(file_path):
    try:
        with open(file_path, 'r', encoding='utf-8') as file:
            return file.read()
    except FileNotFoundError:
        print("File not found.")
        exit(1)

def spinner(done_event):
    spinner = "|/-\\"
    while not done_event.is_set():
        for char in spinner:
            print(f"\rThinking... {char}", end="", flush=True)
            time.sleep(0.1)

def main():
    parser = argparse.ArgumentParser(description="Send a file's content to OpenAI.")
    parser.add_argument("file_path", help="The path to the file whose content will be sent to ChatGPT")
    args = parser.parse_args()
    file_path = args.file_path

    # Prompt the user for the prompt_text
    prompt_text = input("Please enter the prompt text: ")

    file_content = read_file_content(file_path)

    done_event = threading.Event()
    t = threading.Thread(target=spinner, args=(done_event,))
    t.start()

    try:
        responses = send(prompt=prompt_text, text_data=file_content)
    except Exception as e:
        print(f"An error occurred: {e}")
        done_event.set()
        t.join()
        exit(1)

    done_event.set()
    t.join()

    print("\rThinking... Done!")
    for response in responses:
        print(response)

if __name__ == "__main__":
    main()
