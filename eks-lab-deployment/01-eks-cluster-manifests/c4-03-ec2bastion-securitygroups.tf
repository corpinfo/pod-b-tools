# AWS EC2 Security Group Terraform Module
# Security Group for Public Bastion Host
module "public_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "5.1.0"

  name = "${local.name}-public-sg"
  description = "Security Group with SSH port NOT open for everybody (IPv4 CIDR), egress ports are NOT all world open"
  vpc_id = data.terraform_remote_state.eks-vpc.outputs.vpc_id
  # Ingress Rules & CIDR Blocks
  ingress_rules = ["ssh-tcp"]
  ingress_cidr_blocks = ["0.0.0.0/0"]
  # ingress_cidr_blocks = ["72.177.14.68/32","72.32.180.183/32"]
  # Egress Rule - all-all open
  egress_rules = ["all-all"]
  tags = local.common_tags
}