locals {
  dms_table_mappings_file = "${path.module}/dms_mappings.json"
  # # If needing multiple mappings
  # dms_table_mappings_joined = {
  #   mappings = flatten([for fname in local.dms_table_mappings_file: jsondecode(file(fname))["mappings"]])
  # }
  dms_task_settings_file = "${path.module}/dms_task_settings.json"
}