# Terraform Settings Block
terraform {
  required_version = ">= 1.6.0"
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 5.36"
     }
    helm = {
      source = "hashicorp/helm"
      version = "~> 2.12"
    }
    http = {
      source = "hashicorp/http"
      version = "~> 3.4"
    }
  }
  # Adding Backend as S3 for Remote State Storage
  backend "s3" {
    bucket = "terraform-on-aws-eks-n8"
    key    = "ebs-storage/terraform.tfstate"
    region = "ca-central-1"

    # For State Locking
    dynamodb_table = "ebs-storage"
  }
}

# Terraform AWS Provider Block
provider "aws" {
  region = var.aws_region
}

# Terraform HTTP Provider Block
provider "http" {
  # Configuration options
}