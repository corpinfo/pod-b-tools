resource "aws_security_group_rule" "dms-access-rule" {
  description = "Access for DMS instance IP"
  type = "ingress"
  from_port = 5432
  to_port = 5432
  security_group_id = aws_security_group.postgres-replication-test-sg.id
  protocol = "tcp"
  cidr_blocks = ["${aws_dms_replication_instance.replication-test.replication_instance_private_ips[0]}/32"]
}
resource "aws_security_group_rule" "dev-access-rule" {
  description = "Personal Inbound Rule for n8"
  type = "ingress"
  from_port = 5432
  to_port = 5432
  security_group_id = aws_security_group.postgres-replication-test-sg.id
  protocol = "tcp"
  cidr_blocks = ["72.177.26.139/32"]
}
resource "aws_security_group_rule" "postgres_egress_rule" {
  type = "egress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.postgres-replication-test-sg.id
}
resource "aws_security_group_rule" "dms_egress_rule" {
  type = "egress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.dms-replication-test-sg.id
}