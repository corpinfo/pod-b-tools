

(Get-SSMParameter -Name check_windows_services).Value | Out-File (New-Item -Path "C:\temp\monitor.ps1" -Force) -Encoding utf8 

$task="CheckWindowsServicesRunning" 
Get-ScheduledTask -TaskName $task -ErrorAction SilentlyContinue -OutVariable taskStatus1
if ($taskStatus1) { Unregister-ScheduledTask -TaskName $task -Confirm:$false } 


$action = New-ScheduledTaskAction -Execute 'Powershell.exe' -Argument '-NoProfile -WindowStyle Hidden -file "C:\temp\monitor.ps1" ' 
$trigger = New-ScheduledTaskTrigger -Once -At (Get-Date).AddMinutes(5) -RepetitionInterval (New-TimeSpan -Minutes 10) 
$principal = New-ScheduledTaskPrincipal -UserId "NT AUTHORITY\SYSTEM" -LogonType ServiceAccount -RunLevel Highest 
$settings = New-ScheduledTaskSettingsSet -Compatibility At -ExecutionTimeLimit (New-TimeSpan -Minutes 15) 

Register-ScheduledTask -TaskName $task -Action $action -Principal $principal -Trigger $trigger -Settings $settings
