# Jira Template Manager

## Description

Manages and creates as JSON and text files a repeatable structure to create rich, customized, linked tasks and epics within Jira

### Usage

**Requirements**

`export ATLASSIAN_API_KEY=**************` being set for current environment

**Example**

1. Add `./companytemplates/<KEY>.txt` with the base customer template
2. (Optional (2,3): will create and use a task template) Make files for create-review-deploy (* if using bash use '' instead of "" *)
   `python create-issues.py --email msollie@onica.com --key CUST --summary "dockerize frontend" --template tasks/create-review-deploy.json --makebodyfiles`
3. Edit `tasks/body/<task>.txt` templates as desired for customer.
4. create tasks `python create-issues.py --email msollie@onica.com --key CUST --summary "dockerize frontend" --template tasks/create-review-deploy.json`

**Comand**

Make general acceptance criteria templates for tasks:

`python create-issues.py --email me@onica.com --key CUST --summary "TEST TASK" --template tasks/create-review-deploy.json --makebodyfiles`

Make all tasks templates for `CUST` project based on the customer template:

`python create-issues.py --email me@onica.com --key CUST --summary "" --template tasks/create-review-deploy.json --makefullfiles`

Create all tasks in `tasks/create-review-deploy.json` in project `CUST`:

_if `--makebodyfiles` has been run, those templates will take precedence over the company default template or default template_
_if `--makefullfiles` has been run, those templates will take precedence over the company default template or default template_

`python create-issues.py --email msollie@onica.com --key CUST --summary "this specific thing" --template tasks/create-review-deploy.json`

`create-issue.py`

| Argument      | Required | Value                                                                                                           |
| ------------- | -------- | --------------------------------------------------------------------------------------------------------------- |
| email         | yes      | Email of atlassian account related to token                                                                     |
| key           | yes      | `PRJ` type key of destination project                                                                         |
| summary       | yes      | Suffix aded to all descriptions                                                                                 |
| template      | no       | Json path to template to create tasks from                                                                      |
| makebodyfiles | no       | Does not create tasks. create a blank to per-task acceptance criteria templates to be customized in `./body/` |
| makefullfiles | no       | Does not create tasks. copy create customer-specific per-task templates to be customized in `./body/`         |
| plan          | no       | output plan of tasks without creating                                                                           |
| epic          | no       | Assigns an epic ID to the task                                                                                  |

### Structure

`./example/template.json`: templates can be in any recusrsive folder structure

`./example/body/<KEY>-<task-id>.txt`: files output per project key by `--makefullfiles`

`./example/body/<task-id>.txt`: files output per project key by `--makebodyfiles`

`./companytemplates/<KEY>.txt`: individual templates for each customer by project key

*Template Precedence*

1. `./example/body/<KEY>-<task-id>.txt`: per-task + customer template (`--makefullfiles`)
2. `./example/body/<task-id>.txt`: per-task template that appends the defaul customer template (`--makebodyfiles`)
3. `./companytemplates/<KEY>.txt`: customer template
4. `./companytemplates/default.txt` default template
