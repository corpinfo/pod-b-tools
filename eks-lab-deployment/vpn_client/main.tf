resource "aws_ec2_client_vpn_endpoint" "vpn" {
  description               = "VLV-${var.description}-VPN"
  client_cidr_block         = var.cidr_block
  split_tunnel              = true
  server_certificate_arn    = var.server_certificate_arn
  security_group_ids        = [var.client_vpn_sg]
  vpc_id                    = var.vpc_id

  authentication_options {
    type = var.vpn_client_root_arn == null ? "directory-service-authentication" : "certificate-authentication"
    root_certificate_chain_arn  = var.vpn_client_root_arn
    active_directory_id         = var.vpn_client_directory_id
  }

  connection_log_options {
    enabled = false
  }

  tags = {
    "Name"  =   "vlv-${var.description}-vpn-endpoint"
}
}
resource "aws_ec2_client_vpn_network_association" "vpn_subnets" {
  count = length(var.client_vpn_subnet)

  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.vpn.id
  subnet_id              = element(var.client_vpn_subnet, count.index)

  lifecycle {
    ignore_changes = [subnet_id]
  }
}

resource "aws_ec2_client_vpn_authorization_rule" "vpn_auth_rule" {
  client_vpn_endpoint_id  = aws_ec2_client_vpn_endpoint.vpn.id
  target_network_cidr     = "0.0.0.0/0"
  authorize_all_groups    = true
}