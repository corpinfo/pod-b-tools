# Provider and version setup
terraform {
  required_version = ">= v1.0.9"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.21"
    }
  }
}
