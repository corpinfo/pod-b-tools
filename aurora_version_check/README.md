# check-access, login-all, and aurora-check

These scripts allow you to check AWS account access, log in to multiple AWS accounts, and check Aurora version 1 database clusters, respectively, across multiple profiles and regions.

## Prerequisites

- AWS CLI version 2 or higher
- Bash, Zsh, or Fish shell

## Installation

1. Download the scripts to your local machine
2. Make the scripts executable with the following command: `chmod +x check-access login-all aurora-check`
3. (Optional) Move the scripts to a directory in your `PATH` environment variable, so that you can run them from anywhere.
4. Use Onica-SSO multi-login to save credentials to your `~/.aws/credentials` file before running the `login-all` script.

## Usage

### check-access

The `check-access` script allows you to check AWS account access for multiple accounts, to see if 'pod-b' is not listed within the admin user.  This was only really needed to find out which accounts did not have 'pod-b' listed within the admin user account groups.  There are some old accounts that are still listed and the script does no real access test through IAM roles.

#### Bash, Zsh, and Fish:
```
./check-access
```

### login-all

The `login-all` script allows you to log in to multiple AWS accounts across different profiles and regions using Onica-SSO multi-login. This script automatically saves the credentials to your `~/.aws/credentials` file, so you don't have to manually enter them every time you switch between accounts.  It's also already filtering out a list of accounts, which can be modified by altering the `grep` command.

#### Bash, Zsh, and Fish:
```
./login-all
```

This will prompt you to enter your Onica-SSO credentials, and then automatically log you in to all the AWS accounts associated with your profile. You can then use the `check-access` and `aurora-check` scripts to check access and database clusters across these accounts.

### aurora-check

The `aurora-check` script allows you to check Aurora version 1 database clusters for multiple profiles and regions.

#### Bash, Zsh, and Fish:
```
./aurora-check
```

By default, the script runs the command for the US and CA regions. If you need to check other regions, you can modify the `regions` variable in the script.

