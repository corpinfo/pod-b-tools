# Audit for security policies on v2 load balancers
#
# To-do - regions, output to .csv , describe_ssl_policies
#
# Classic load balancers have a different format, and the SP's are buried in 'Other Policies' (which can contain multiple unrelated policies), unlike the v2 which have are in 'SslPolicies'. 
# Ciphers would also be nice, maybe something to include in csv output. This can be done programmatically, see - describe_ssl_policies. Looks like it's a better way of getting the TLS version
# as well. No need to keep a static list around if describe_ssl_policies is implemneted instead.


import boto3

# TLS 1.0 and 1.1 supported policies
old_pol = ['ELBSecurityPolicy-FS-2018-06', 'ELBSecurityPolicy-2016-08', 'ELBSecurityPolicy-FS-1-1-2019-08', 'ELBSecurityPolicy-TLS-1-1-2017-01', 'ELBSecurityPolicy-TLS-1-0-2015-04']

# ports we want to ignore
port_ignore = [80, 22]

client = boto3.client('elbv2')
response = client.describe_load_balancers()

for elb in response['LoadBalancers']:
    lb_arn = elb['LoadBalancerArn']
    lb_name = elb['LoadBalancerName']
    response2 = client.describe_listeners(LoadBalancerArn=lb_arn)
    for elb2 in response2['Listeners']:
        l_arn = elb2['ListenerArn']
        l_port = elb2['Port']
        response3 = client.describe_listeners(ListenerArns=[l_arn])
        for elb3 in response3['Listeners']:
            # Let's toss out anything running on ports 80 or 22
            if l_port in port_ignore:
                pass
            else:    
                try:
                    lbp = elb3['SslPolicy']
                    if lbp in old_pol:
                        print('LB Name:',lb_name, '   Policy: \033[91m WARNING! Old Version detected! \033[00m', '\033[91m', lbp ,'\033[00m', '\n \033[94m    --- Listener ARN: ' ,l_arn, '\033[00m' )
                    else:    
                        print('LB_Name:',lb_name, '   Policy:', elb3['SslPolicy'])
                 # Things will barf if there is a Listener without a policy so instead lets catch it and print out the arn and port of the offender       
                except KeyError:
                    print('LB : ' , lb_name ,  ' Policy : None ' , 'LISTENER ARN : ' , l_arn , 'Port : ' , l_port)









