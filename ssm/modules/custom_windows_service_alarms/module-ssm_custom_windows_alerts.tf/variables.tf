variable "eventlog_group_name" {
   description = "The event log group name for the metric"
   type        = string
   default     = "Test/Windows/EventLogServiceStatus"
}

variable "spoolerlog_group_name" {
   description = "The spooler log group name for the metric"
   type        = string
   default     = "Test/Windows/SpoolerServiceStatus"
}
