resource "aws_iam_user" "pipeline_user" {
  name = "${local.unique_prefix}-rackspace-pipeline-user"
  path = "/"
  tags = {
    Description = "Used for Onica/Rackspace statefile creation"
  }
}

resource "aws_iam_user_policy_attachment" "pipeline_user_attach" {
  user       = aws_iam_user.pipeline_user.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}
