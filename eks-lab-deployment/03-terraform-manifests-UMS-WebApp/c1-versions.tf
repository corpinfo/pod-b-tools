# Terraform Settings Block
terraform {
  required_version = ">= 1.6.0"
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 5.36"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = ">= 2.25"
    }
  }
  # Adding Backend as S3 for Remote State Storage
  backend "s3" {
    bucket = "terraform-on-aws-eks-n8"
    key    = "ebs-sampleapp-demo/terraform.tfstate"
    region = "ca-central-1"

    # For State Locking
    dynamodb_table = "ebs-sampleapp-demo"
  }
}

