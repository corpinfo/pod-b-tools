#!/bin/bash

# Extract all profile names from the credentials file
profiles=($(grep -E '^\[.*\]' ~/.aws/credentials | tr -d '[]')))

# Loop through all profiles
for profile in "${profiles[@]}"
do
  # Define the regions to include
  regions=$(aws ec2 describe-regions --region us-east-1 --profile $profile | jq -r '.Regions[] | .RegionName')
  # Loop through all regions
  for region in "${regions[@]}"
  do
    echo "Profile: $profile, Region: $region"
    # Run the command for the profile and region
    aws rds describe-db-clusters --include-share --query 'DBClusters[?Engine==`aurora`].{EV:EngineVersion, DBCI:DBClusterIdentifier, EM:EngineMode}' --output table --profile "$profile" --region "$region"
  done
done

