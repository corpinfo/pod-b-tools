#!/bin/bash

# Check if the ~/.aws/credentials file exists
if [ -e ~/.aws/credentials ]; then
  # If it exists, rename it with a timestamp
  timestamp=$(date +%Y%m%d_%H%M%S)
  mv ~/.aws/credentials ~/.aws/credentials.$timestamp
fi

account_list=$(onica-sso list accounts | awk '{print $1}' | grep -Ev "(spireon|teva|djo|rxvantage|delos|centrak|atvenu|ssense|big-ass-fan|ong-lz|onica-aag|dane-street|onica-informeddna|opta-gistestenv|onica-mediaservices|onica-member365|courtcall-video-ro|zodiac-iot)")

# Run the sso multi-login command and output to the ~/.aws/credentials file
sso multi-login -o ~/.aws/credentials $account_list --verbose

