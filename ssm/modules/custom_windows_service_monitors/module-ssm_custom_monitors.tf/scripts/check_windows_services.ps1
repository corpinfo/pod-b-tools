$thisInstance = Get-EC2InstanceMetadata -Category InstanceId;
$Timestamp = (Get-Date).ToUniversalTime();

$servicesToMonitor = @("Spooler", "EventLog")


foreach ($service in $servicesToMonitor) {
    $serviceStatus = Get-Service $service

New-Item -ItemType Directory -Path "C:\temp" -Force

$myService = Get-Service -Name $service;
$ServiceName = ($myService.DisplayName).replace(' ','')

"$Timestamp $thisInstance $ServiceName $($myService.Status)" | Out-File "C:\temp\$service-status.log" -Append -Encoding utf8;
  
}
