*Acceptance Criteria*
* Find all Ec2 instances in the stack
* Identify appropriate metrics to monitor on each such as:
* Memory
* Disk (letters/devices)
* Logs to ship to CW Logs
* Processes that procstat metrics could monitor
* Note configurations in linked creation task for configurations to create