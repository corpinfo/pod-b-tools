#! /usr/local/bin/python3
import argparse
import requests
import copy
import glob
import json
import boto3
import time
import os

from requests.auth import HTTPBasicAuth


def parse_opts():
    """Parse CLI Options."""
    parser = argparse.ArgumentParser(
        description='Create Jira tasks from templates'
    )

    parser.add_argument(
        '--email',
        help='The user email',
        required=True
    )

    parser.add_argument(
      '--epic',
      help='Epic ID to link tasks to',
    )

    parser.add_argument(
      '--makefullfiles',
      help='make body files for the json data',
      action='store_true',
    )

    parser.add_argument(
      '--makebodyfiles',
      help='make body files for the json data',
      action='store_true',
    )

    parser.add_argument(
        '--key',
        help='The customer key in Jira (2 or 3 letters)',
        required=True
    )

    parser.add_argument(
        '--summary',
        help='short description summary',
        required=True
    )

    parser.add_argument(
        '--template',
        help='template name'
    )

    parser.add_argument(
        '--plan',
        help='plan output only',
        action='store_true',
    )

    args = parser.parse_args()
    return args

def get_templates():
      files = []
      for r, d, f in os.walk("."):
          for file in f:
              if '.json' in file:
                  files.append(os.path.join(r, file).lstrip('./'))
      return files
      for f in files:
          print(f)

def get_meta(key, auth):
    url = "{}/issue/createmeta?expand=projects.issuetypes.fields&projectKeys={}&issuetypeNames=Task".format(BASE_URL, key)

    response = requests.request(
      "GET",
      url,
      auth=auth,
      headers=HEADERS
    )

    print(json.dumps(json.loads(response.text), sort_keys=True, indent=4, separators=(",", ": ")))


def get_template(path):
  with open(path) as json_file:
      try:
        data = json.load(json_file)
      except:
        data = None
      return data

def get_company_template(key, body_path, task):

  # try 1
  try:
    template_path = "{}/{}-{}.txt".format(body_path, key, task)
    with open(template_path) as t_file:
      data = t_file.read()
  except Exception as e:
    data = None
    #try 2
    try:
      with open('companytemplates/{}.txt'.format(key)) as t_file:
        base_template = t_file.read()
    except Exception as e:
      #print("EXCEPTION: get_company_template. try 2")
      with open('companytemplates/default.txt') as t_file:
        base_template = t_file.read()
    # try 3
    try:
      template_path = "{}/{}.txt".format(body_path, task)
      with open(template_path) as t_file:
        data = t_file.read() + "\n\n" + base_template
    except Exception as e:
      #print("EXCEPTION: get_company_template. try 3")
      data = base_template

  return data


def create_jira(payload, auth):
    url = "{}/issue".format(BASE_URL)

    response = requests.request(
      "POST",
      url,
      data=json.dumps(payload),
      auth=auth,
      headers=HEADERS
    )
    #print response.reason
    #print response.text
    response.raise_for_status()
    #print(json.dumps(json.loads(response.text), sort_keys=True, indent=4, separators=(",", ": ")))
    return json.loads(response.text)


def update_issue(issue_number, payload, auth):
    url = "{}/issue/{}".format(BASE_URL,issue_number)


    response = requests.request(
      "PUT",
      url,
      data=json.dumps(payload),
      auth=auth,
      headers=HEADERS
    )
    #print response.reason
    #print response.text
    response.raise_for_status()
    #print(json.dumps(json.loads(response.text), sort_keys=True, indent=4, separators=(",", ": ")))
    return


def get_edit_issue_meta(issueNumber):

    url = "{}/issue/{}/editmeta".format(BASE_URL,issueNumber)

    response = requests.request(
      "GET",
      url,
      headers=HEADERS
    )

    print(json.dumps(json.loads(response.text), sort_keys=True, indent=4, separators=(",", ": ")))

########################################################################################################################

def main():

  args = parse_opts()
  auth = HTTPBasicAuth(args.email, api_key)
  if not args.template:
    print ("please specify a template from the following")
    for f in get_templates():
        print(f)
  else:
    tasks = get_template(args.template)


    created_tasks = {
      "meta": {}
    }

    for task in tasks:
      created_tasks[task['meta']['id']] = {
        "links":{
          "blocked_by":[]
        }
      }

    body_path="{}/body".format(args.template.rpartition('/')[0])

    if not os.path.exists(body_path):
      os.mkdir(body_path)

    for i in range(0,len(tasks)):
      task = tasks[i]
      task_meta = task.pop("meta")
      task_type = task['fields']['issuetype']['name']
      company_template = get_company_template(args.key, body_path, task_meta['id'])
      if task_type == 'Epic':
        if task['fields']['summary'] != "":
          task['fields']['customfield_10008'] = "{} {}".format(task['fields']['summary'], args.summary)
        else:
          task['fields']['customfield_10008'] = args.summary

      if args.epic:
        epic = args.epic
      elif task_meta.get('epic-id'):
        epic = task_meta.get('epic-id')
      else:
        epic = created_tasks["meta"].get("epic", None)

      if epic and task_type != 'Epic':
        task['fields']['customfield_10007'] = epic
      if args.makefullfiles:
        f = open("{}/{}-{}.txt".format(body_path, args.key, task_meta['id']),"w+")
        f.write('*Acceptance Criteria*')
        f.write(ac_template)
        f.close()
      elif args.makebodyfiles:
        if not os.path.exists("{}/{}.txt".format(body_path, task_meta['id'])):
          f = open("{}/{}.txt".format(body_path, task_meta['id']),"w+")
          f.write('*Acceptance Criteria*')
          f.close()
      else:
        print ("creating task {} of {}".format(i+1, len(tasks)))
        if task['fields']['summary'] != "":
          task['fields']['summary'] = "{} {}".format(task['fields']['summary'], args.summary )
        else:
          task['fields']['summary'] = args.summary

        task['fields']['description']['content'][0]['content'][0]['text'] = task['fields']['description']['content'][0]['content'][0]['text'] + "\n\n" + company_template
        task['fields']['project'] = {"key": args.key}

        if args.plan is True:
          print(json.dumps(task, sort_keys=True, indent=4))
        else:
          try:
            response = create_jira(task, auth)
            print ("Created: {} - {}".format(response['key'], task['fields']['summary']))
          except Exception as e:
            print ("Error on creation: {}".format(task['fields']['summary']))
            print (e)
            exit()
          created_tasks[task_meta['id']]["data"] = response
          if task_type == 'Epic':
            created_tasks["meta"] = {"epic": response['key']}

          for link in task_meta['blocks']:
            created_tasks[link]["links"]["blocked_by"].append(response['key'])

    if args.plan is True:
      print ("PLAN: skipping linking")
    else:
      for task in created_tasks:
        if task != "meta":
          for link in created_tasks[task]["links"]["blocked_by"]:
            print ("linking {} to {}".format(link, task))
            task_updates = copy.deepcopy(LINKS)
            task_updates['update']['issuelinks'][0]['add']['inwardIssue']={"key": link}
            update_issue(created_tasks[task]['data']['key'], task_updates, auth)


if __name__ == '__main__':
  try:
    api_key = os.environ['ATLASSIAN_API_KEY']
  except:
    print ("Please Set ATLASSIAN_API_KEY environment variable before continuing.")
    exit()
  BASE_URL = 'https://nbdevs.atlassian.net/rest/api/3'
  HEADERS = {
        "Accept": "application/json",
        "Content-Type": "application/json",
      }
  LINKS = {
      "update":{
        "issuelinks":[
          {
            "add":{
              "type":{
                  "name":"Blocks",
                  "inward":"is blocked by",
                  "outward":"blocks"
              },
            }
          }
        ]
      }
    }
  main()
