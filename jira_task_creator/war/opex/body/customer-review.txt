*Acceptance Criteria*
* Review with customer recomendations from the discovery task(s)
* Decide on appropriate actions to remediate, and what may be acceptable as is
* Add Acceptance Criteria to the linked implementation task
