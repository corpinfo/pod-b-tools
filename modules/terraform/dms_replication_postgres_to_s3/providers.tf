# Provider and version setup
terraform {
  required_version = ">= v1.0.9"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.21"
    }
  }

  backend "s3" {
    region = "us-east-1"
    bucket = "7f28becbdbd4f34369f127b0e02f185d-tfstate"
    key = "snowflake_replication.tfstate"
    dynamodb_table = "7f28becbdbd4f34369f127b0e02f185d-tfstate-lock-table"
  }
}
