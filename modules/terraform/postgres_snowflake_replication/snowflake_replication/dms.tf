# Subnet Group for DMS
resource "aws_dms_replication_subnet_group" "replication-test" {
  replication_subnet_group_description = "Test replication subnet group for DMS Instance"
  replication_subnet_group_id          = "test-dms-replication-subnet-group"

  subnet_ids = [
    "subnet-d463c8ea",
    "subnet-897a18a7",
    "subnet-8f086fe8",
    "subnet-d769098b",
    "subnet-202e196a",
    "subnet-4f267f40"
  ]

  tags = {
    Name = "replication-test-group"
  }
}

# Create a new replication instance
resource "aws_dms_replication_instance" "replication-test" {
  allocated_storage            = 50
  apply_immediately            = true
  auto_minor_version_upgrade   = true
  availability_zone            = aws_db_instance.snowflake-replication-test.availability_zone
  engine_version               = "3.4.7"
  # kms_key_arn                  = aws_kms_key.dms.arn
  multi_az                     = false
  # preferred_maintenance_window = "sun:10:30-sun:14:30"
  publicly_accessible          = true
  replication_instance_class   = "dms.t3.micro"
  replication_instance_id      = "test-dms-replication-instance"
  replication_subnet_group_id  = aws_dms_replication_subnet_group.replication-test.id

  tags = {
    Name = "replication-test"
  }

  vpc_security_group_ids = [
    aws_security_group.postgres-replication-test-sg.id
  ]

  depends_on = [
    aws_iam_role_policy_attachment.dms-cloudwatch-logs-role-AmazonDMSCloudWatchLogsRole,
    aws_iam_role_policy_attachment.dms-vpc-role-AmazonDMSVPCManagementRole
  ]
}

# Replication Source endpoint
resource "aws_dms_endpoint" "dms-endpoint-source" {
  database_name               = "testdb"
  endpoint_id                 = "test-dms-endpoint-source"
  endpoint_type               = "source"
  engine_name                 = "postgres"
  extra_connection_attributes = "addColumnName=true;heartbeatEnable=1;"
  password                    = data.aws_ssm_parameter.rds_password.value
  port                        = 5432
  server_name                 = trimsuffix(aws_db_instance.snowflake-replication-test.endpoint,":5432")
  ssl_mode                    = "none"
  tags = {
    Name = "dms-source-test"
  }
  username = data.aws_ssm_parameter.rds_username.value
}

# Create Source Endpoint to Postgres DB
resource "aws_dms_endpoint" "dms-endpoint-target" {
  endpoint_id                 = "test-dms-endpoint-target"
  endpoint_type               = "target"
  engine_name                 = "s3"
  tags = {
    Name = "dms-target-test"
  }
  s3_settings {
    bucket_name                 = aws_s3_bucket.snowflake-replication-bucket.id
    data_format                 = "csv"
    date_partition_enabled      = false
    encryption_mode             = "SSE_S3"
    service_access_role_arn     = aws_iam_role.dms-cdc-test.arn
    # csv_delimiter               = ","
    # csv_row_delimiter           = "\\n"
    timestamp_column_name       = "cdctimestamp"
    # use_task_start_time_for_full_load_timestamp = true
  }
}

# Create a new replication task
resource "aws_dms_replication_task" "postgres-cdc-task" {
  replication_task_id       = "test-dms-replication-task"
  start_replication_task    = true
  migration_type            = "full-load-and-cdc"
  # cdc_start_time            = 1665694706
  replication_instance_arn  = aws_dms_replication_instance.replication-test.replication_instance_arn
  replication_task_settings = jsonencode(jsondecode(file(local.dms_task_settings_file)))
  source_endpoint_arn       = aws_dms_endpoint.dms-endpoint-source.endpoint_arn
  target_endpoint_arn       = aws_dms_endpoint.dms-endpoint-target.endpoint_arn
  table_mappings            = jsonencode(jsondecode(file(local.dms_table_mappings_file)))
  # table_mappings            = "{\"rules\":[{\"rule-type\":\"selection\",\"rule-id\":\"1\",\"rule-name\":\"1\",\"object-locator\":{\"schema-name\":\"%\",\"table-name\":\"%\"},\"rule-action\":\"include\"}]}"

  tags = {
    Name = "test-postgres-cdc-task"
  }
}