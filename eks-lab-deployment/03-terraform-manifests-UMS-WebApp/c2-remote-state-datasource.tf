# Terraform Remote State Datasource - Remote Backend AWS S3
data "terraform_remote_state" "eks" {
  backend = "s3"
  config = {
    bucket = "terraform-on-aws-eks-n8"
    key    = "env:/${terraform.workspace}/eks-cluster/terraform.tfstate"
    region = "ca-central-1"
  }
}