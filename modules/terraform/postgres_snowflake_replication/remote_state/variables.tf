variable "customer" {
  type        = string
  description = "A unique identifier to differentiate this deployment."
}

variable "application" {
  type        = string
  description = "A unique identifier to differentiate this deployment."
}

variable "region" {
  type        = string
  description = "The region in which to deploy the remote backend resources."
}

variable "environment" {
  type        = string
  description = "Environment name, such as 'dev', 'test', or 'prod'"
}

variable "abort_incomplete_multipart_upload_days" {
  type        = number
  description = "The number of days to wait before deleting incomplete multipart uploads."
  default     = 10
}

variable "tfstate_noncurrent_version_expiration_days" {
  type        = number
  description = "The number of days to wait before deleting noncurrent objects from the tfstate bucket."
}

variable "tfstate_access_logs_expiration_days" {
  type        = number
  description = <<-EOT
  The number of days to wait before deleting tfstate access logs.
  This number must be greater than var.tfstate_access_logs_transition_days
  EOT
}

variable "tfstate_access_logs_transition_storage_class" {
  type        = string
  description = "The storage class to which access logs should be moved to."
}

variable "tfstate_access_logs_transition_days" {
  type        = number
  description = <<-EOT
  The number of days to wait before moving access logs to the storage class
  defined by var.tfstate_access_logs_transition_storage_class. This number
  must be less than var.tfstate_access_logs_expiration_days. This number
  must also adhere to the constraints documented here:
  https://docs.aws.amazon.com/AmazonS3/latest/userguide/lifecycle-transition-general-considerations.html
  which are dependent on which storage class is passed in for
  var.tfstate_access_logs_transition_storage_class
  EOT
}
