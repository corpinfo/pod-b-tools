resource "aws_ssm_parameter" "state-bucket" {
  name = "/snowflake/state_bucket"
  type = "String"
  value = aws_s3_bucket.tfstate.id
}
resource "aws_ssm_parameter" "state-table" {
  name = "/snowflake/state_table"
  type = "String"
  value = aws_dynamodb_table.terraform_state_lock_table.id
}