module "ssm_custom_alarms" {

  source                                                                  = "./module-ssm_custom_windows_alerts.tf"
}

provider "aws" {
}


terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }

  }

  required_version = ">= 1.0"
}

