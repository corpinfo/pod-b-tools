from __future__ import annotations

import os
import zipfile

from typing import TYPE_CHECKING, Any, Dict, Optional

import boto3

from runway.context import CfnginContext

def s3_upload(context: CfnginContext, bucket_name: str, s3_prefix: str, **kwargs: Any) -> Optional[Dict[str, Any]]:
    """
    A runway hook that uploads a directory to S3 and creates .zip files for each file within `Lambda`.
    """
    # Create S3 client
    s3 = boto3.client('s3')

    # Directories to zip
    lambda_dirs = ['access_key_auto_rotation', 'account_inventory', 'notifier']

    # Get the root directory of the project
    start_path = os.path.abspath(os.path.join(os.path.abspath(__file__), '..', '..', 'Lambda'))

    zip_files = []

    # Zip each directory within `Lambda`
    for lambda_dir in lambda_dirs:
        path_to_zip = os.path.join(start_path, lambda_dir)
        if not os.path.isdir(path_to_zip):
            raise Exception(f"{path_to_zip} is not a directory")

        zip_path = path_to_zip + ".zip"
        with zipfile.ZipFile(zip_path, 'w', zipfile.ZIP_DEFLATED) as zip_file:
            for root, dirs, files in os.walk(path_to_zip):
                for file in files:
                    file_path = os.path.join(root, file)
                    zip_file.write(file_path, os.path.join(root[len(path_to_zip)+1:], file))

        zip_files.append(zip_path)

    # Upload the `.zip` files within the `Lambda` directory to the S3 bucket
    s3_dir = s3_prefix + "/Lambda"
    for zip_file in zip_files:
        file_name = os.path.basename(zip_file)
        s3_path = os.path.join(s3_dir, file_name)
        # s3.upload_file(zip_file, bucket_name, s3_path)
        print("Uploaded {} to {}".format(zip_file, s3_path))

    # Remove the `.zip` files within the `Lambda` directory
    for zip_file in zip_files:
        os.remove(zip_file)

    # Return success message
    return {"result": "Upload successful."}

if __name__ == "__main__":
    bucket_name = "cfn-templates-lambda-iam-key-rotation"
    s3_prefix = "asa/asa-iam-rotation"
    result = s3_upload(None, bucket_name, s3_prefix)
    print(result)
