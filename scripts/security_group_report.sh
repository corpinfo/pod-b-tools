#!/bin/bash

security_groups=$(aws ec2 describe-security-groups --query 'SecurityGroups[].{GroupId: GroupId, GroupName: GroupName}' --output json --region us-west-2)

for row in $(echo "${security_groups}" | jq -r '.[] | @base64'); do
  _jq() {
    echo "${row}" | base64 --decode | jq -r "${1}"
  }

  group_id=$(_jq '.GroupId')
  group_name=$(_jq '.GroupName')

  sg_details=$(aws ec2 describe-security-groups --group-ids "${group_id}" --output json --region us-west-2)

  ingress_rules=$(echo "${sg_details}" | jq -r '.SecurityGroups[].IpPermissions[] | {Type: "Ingress", IpProtocol: .IpProtocol, FromPort: .FromPort, ToPort: .ToPort, IpRanges: [.IpRanges[].CidrIp], UserIdGroupPairs: [.UserIdGroupPairs[].GroupId]}' | jq -s '.' | sed 's/}/}\n/g')
  egress_rules=$(echo "${sg_details}" | jq -r '.SecurityGroups[].IpPermissionsEgress[] | {Type: "Egress", IpProtocol: .IpProtocol, FromPort: null, ToPort: null, IpRanges: [.IpRanges[].CidrIp], UserIdGroupPairs: [.UserIdGroupPairs[].GroupId]}' | jq -s '.' | sed 's/}/}\n/g')

  instances=$(aws ec2 describe-instances --filters "Name=network-interface.group-id,Values=${group_id}" --query 'Reservations[].Instances[].[InstanceId, PrivateIpAddress, Tags[?Key==`Name`].Value | [0]]' --output json --region us-west-2 | jq -r 'map(["Instance ID: \(.[0])", "Private IP: \(.[1])", "Hostname: \(.[2])"])' | sed 's/]/]\n/g')
  interfaces=$(aws ec2 describe-network-interfaces --filters "Name=group-id,Values=${group_id}" --query 'NetworkInterfaces[].[InterfaceType, NetworkInterfaceId, PrivateIpAddress, Description]' --output json --region us-west-2 | jq -r 'map(["Interface Type: \(.[0])", "Interface ID: \(.[1])", "Private IP: \(.[2])", "Description: \(.[3])"])' | sed 's/]/]\n/g')

  resources=()
  if [ -n "${instances}" ]; then
    resources+=("Instances:")
    resources+=("${instances}")
  fi

  if [ -n "${interfaces}" ]; then
    resources+=("Interfaces:")
    resources+=("${interfaces}")
  fi

  if [ -z "${instances}" ] && [ -z "${interfaces}" ]; then
    resources+=("No instances or network interfaces associated with the security group.")
  fi

  resources+=("Ingress Rules:")
  resources+=("${ingress_rules}")
  resources+=("Egress Rules:")
  resources+=("${egress_rules}")

  printf "GroupId: %s\tGroupName: %s\tResources:\n" "$group_id" "$group_name"
  printf "%s\n" "${resources[@]}"
done
