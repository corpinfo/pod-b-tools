# Create a Null Resource and Provisioners
resource "terraform_data" "copy_ec2_keys" {
  depends_on = [module.ec2_public]
  count = length(aws_eip.bastion_eip) > 0 ? 1 : 0
  # Connection Block for Provisioners to connect to EC2 Instance
  connection {
    type     = "ssh"
    host     = aws_eip.bastion_eip[0].public_ip
    user     = "ec2-user"
    password = ""
    private_key = file("../../n8s-pod-b-lab-key.pem")
  }

## File Provisioner: Copies the terraform-key.pem file to /tmp/terraform-key.pem
  provisioner "file" {
    source      = "../../n8s-pod-b-lab-key.pem"
    destination = "/tmp/n8s-pod-b-lab-key.pem"
  }
## Remote Exec Provisioner: Using remote-exec provisioner fix the private key permissions on Bastion Host
  provisioner "remote-exec" {
    inline = [
      "sudo chmod 400 /tmp/n8s-pod-b-lab-key.pem"
    ]
  }
## Local Exec Provisioner:  local-exec provisioner (Creation-Time Provisioner - Triggered during Create Resource)
  provisioner "local-exec" {
    command = "echo VPC created on `date` and VPC ID: ${data.terraform_remote_state.eks-vpc.outputs.vpc_id} >> creation-time-vpc-id.txt"
    working_dir = "local-exec-output-files/"
    #on_failure = continue
  }

}