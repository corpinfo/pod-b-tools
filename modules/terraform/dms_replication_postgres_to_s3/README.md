<!-- BEGIN_TF_DOCS -->

## Requirements

| Name                                                                       | Version   |
| -------------------------------------------------------------------------- | --------- |
| `<a name="requirement_terraform"></a>` [terraform](#requirement\_terraform) | >= v1.0.9 |
| `<a name="requirement_aws"></a>` [aws](#requirement\_aws)                   | ~> 4.21   |

## Providers

| Name                                               | Version |
| -------------------------------------------------- | ------- |
| `<a name="provider_aws"></a>` [aws](#provider\_aws) | 4.34.0  |

## Modules

No modules.

## Resources

| Name                                                                                                                                                                             | Type        |
| -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| [aws_db_instance.snowflake-replication-test](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance)                                               | resource    |
| [aws_db_parameter_group.snowflake_replication](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_parameter_group)                                      | resource    |
| [aws_dms_endpoint.dms-endpoint-source](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dms_endpoint)                                                    | resource    |
| [aws_dms_endpoint.dms-endpoint-target](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dms_endpoint)                                                    | resource    |
| [aws_dms_replication_instance.replication-test](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dms_replication_instance)                               | resource    |
| [aws_dms_replication_subnet_group.replication-test](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dms_replication_subnet_group)                       | resource    |
| [aws_dms_replication_task.postgres-cdc-task](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dms_replication_task)                                      | resource    |
| [aws_iam_role.dms-cdc-test](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role)                                                                   | resource    |
| [aws_iam_role.dms-vpc-role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role)                                                                   | resource    |
| [aws_iam_role_policy_attachment.dms-access-for-endpoint-AmazonS3FullAccess](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource    |
| [aws_iam_role_policy_attachment.dms-vpc-role-AmazonDMSVPCManagementRole](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment)    | resource    |
| [aws_s3_bucket.snowflake-replication-bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket)                                                 | resource    |
| [aws_security_group.dms-replication-test-sg](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group)                                            | resource    |
| [aws_security_group.postgres-replication-test-sg](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group)                                       | resource    |
| [aws_security_group_rule.dev-access-rule](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule)                                          | resource    |
| [aws_security_group_rule.dms-access-rule](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule)                                          | resource    |
| [aws_security_group_rule.dms_egress_rule](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule)                                          | resource    |
| [aws_security_group_rule.postgres_egress_rule](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule)                                     | resource    |
| [aws_iam_policy_document.dms_assume_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document)                                       | data source |
| [aws_ssm_parameter.rds_password](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ssm_parameter)                                                      | data source |
| [aws_ssm_parameter.rds_username](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ssm_parameter)                                                      | data source |

## Inputs

| Name                                                                 | Description                                                 | Type       | Default | Required |
| -------------------------------------------------------------------- | ----------------------------------------------------------- | ---------- | ------- | :------: |
| `<a name="input_application"></a>` [application](#input\_application) | A unique identifier to differentiate this deployment.       | `string` | n/a     |   yes   |
| `<a name="input_customer"></a>` [customer](#input\_customer)          | A unique identifier to differentiate this deployment.       | `string` | n/a     |   yes   |
| `<a name="input_environment"></a>` [environment](#input\_environment) | Environment name, such as 'dev', 'test', or 'prod'          | `string` | n/a     |   yes   |
| `<a name="input_region"></a>` [region](#input\_region)                | The region in which to deploy the remote backend resources. | `string` | n/a     |   yes   |
| `<a name="input_vpc_id"></a>` [vpc\_id](#input\_vpc\_id)              | VPC for porstgres replication                               | `string` | n/a     |   yes   |

## Outputs

| Name                                                                                                                            | Description |
| ------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| `<a name="output_dms_ip"></a>` [dms\_ip](#output\_dms\_ip)                                                                       | n/a         |
| `<a name="output_dms_replication_subnet_group"></a>` [dms\_replication\_subnet\_group](#output\_dms\_replication\_subnet\_group) | n/a         |
| `<a name="output_endpoint"></a>` [endpoint](#output\_endpoint)                                                                   | n/a         |
| `<a name="output_replication_bucket"></a>` [replication\_bucket](#output\_replication\_bucket)                                   | n/a         |

<!-- END_TF_DOCS -->
