# EKS Training repo for n8

> This deployment does not work on appgate, but if you use the lab account, you still have to login to appgate temporarily to grab the session keys.

Run the steps in order, to deploy the cluster.  Some things are still being modified as time goes on.

`make deploy-vpc` deploys the vpc.  If you would like to leave the VPC running, feel free, just set the nat gateway parameters in runway.yml to the following:

vpc_enable_nat_gateway: "false"
vpc_single_nat_gateway: "false"

`make deploy-cluster` deploys the infrastructure for the cluster.  Currently, I believe there's an issue with adding the OIDC provider at the same time as the cluster, so start with the `oidc_provider_enabled` flag set to "false" on your first run, then enable it with "true" after that.

>  Currently, there are no additional EKS cluster security groups, so the eks resource creates a new security group open to the world on port 22.  It's advisable to manually change that for now, until a separate security group is defined to attach and use (coming in the future soon).
