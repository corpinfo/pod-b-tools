# Test Postgres Database Credentials
data "aws_ssm_parameter" "rds_username" {
  name = "/snowflake-test/rds_username"
}

data "aws_ssm_parameter" "rds_password" {
  name = "/snowflake-test/rds_password"
}