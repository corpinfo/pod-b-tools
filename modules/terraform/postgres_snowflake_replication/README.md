# Snowflake Replication to s3

A small integration of replication from a postgres instance to an s3 bucket, that can then be picked up by snowflake.

## Requirements

- Onica Runway (https://docs.onica.com/projects/runway/en/stable/installation.html)
- Terraform (https://learn.hashicorp.com/tutorials/terraform/install-cli)

## Deployment

To deploy this project run the command below in this directory.

```bash
  make deploy-${env}
```

This will run the target that has been setup in `the Makefile`. Additional targets can be added for additional environments, by setting the `DEPLOY_ENVIROMENT` in each new target.

## Destruction

To destroy the resources, run the command below.

```bash
  make destroy-${env}
```

This will destroy all resources, but may get caught up on the s3 bucket and network interface attached to the security group.  Deleting the interface and emptying the bucket will resolve the issue, at which time destruction can complete.
