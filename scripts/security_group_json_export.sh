#!/bin/bash

# Retrieve security group details
security_groups=$(aws ec2 describe-security-groups --query 'SecurityGroups[].{GroupId: GroupId, GroupName: GroupName}' --output json --region us-west-2)

# Start the JSON array
echo "["

# Flag to track the first group
first_group=true

# Loop through each security group
for row in $(echo "${security_groups}" | jq -r '.[] | @base64'); do
  _jq() {
    echo "${row}" | base64 --decode | jq -r "${1}"
  }

  # Retrieve security group ID and name
  group_id=$(_jq '.GroupId')
  group_name=$(_jq '.GroupName')

  # Retrieve security group details
  sg_details=$(aws ec2 describe-security-groups --group-ids "${group_id}" --output json --region us-west-2)

  # Process security group details
  ingress_rules=$(echo "${sg_details}" | jq -r '.SecurityGroups[].IpPermissions[] | {Type: "Ingress", IpProtocol: .IpProtocol, FromPort: .FromPort, ToPort: .ToPort, IpRanges: [.IpRanges[].CidrIp], UserIdGroupPairs: [.UserIdGroupPairs[].GroupId]}')
  egress_rules=$(echo "${sg_details}" | jq -r '.SecurityGroups[].IpPermissionsEgress[] | {Type: "Egress", IpProtocol: .IpProtocol, FromPort: null, ToPort: null, IpRanges: [.IpRanges[].CidrIp], UserIdGroupPairs: [.UserIdGroupPairs[].GroupId]}')

  # Retrieve instance information for the security group
  instances=$(aws ec2 describe-instances --filters "Name=network-interface.group-id,Values=${group_id}" --query 'Reservations[].Instances[].[InstanceId, PrivateIpAddress, Tags[?Key==`Name`].Value | [0]]' --output json --region us-west-2 | jq -r 'map(["Instance ID: \(.[0])", "Private IP: \(.[1])", "Hostname: \(.[2])"])')

  # Retrieve network interface information for the security group
  interfaces=$(aws ec2 describe-network-interfaces --filters "Name=group-id,Values=${group_id}" --query 'NetworkInterfaces[].[InerfaceType, NetworkInterfaceId, PrivateIpAddress, Description]' --output json --region us-west-2 | jq -r 'map(["Inerface Type: \(.[0])", "Interface ID: \(.[1])", "Private IP: \(.[2])", "Description: \(.[3])"])')

  # Combine instance, network interface, and security group rule information
  resources=()
  resources+=("${instances[@]}")
  resources+=("${interfaces[@]}")
  resources+=("Ingress Rules: [${ingress_rules}]")
  resources+=("Egress Rules: [${egress_rules}]")

  # Output the result for each security group as JSON
  if "$first_group"; then
    first_group=false
  else
    echo ","
  fi

  printf '{"GroupId": "%s", "GroupName": "%s", "Resources": %s}' "$group_id" "$group_name" "$(printf '%s\n' "${resources[@]}" | jq -sR .)"
done

# End the JSON array
echo "]"
