# # Database Migration Service requires the below IAM Roles to be created before
# # replication instances can be created. See the DMS Documentation for
# # additional information: https://docs.aws.amazon.com/dms/latest/userguide/CHAP_Security.html#CHAP_Security.APIRole
# #  * dms-vpc-role
# #  * dms-cloudwatch-logs-role (if you want cloudwatch enabled)
# #  * dms-access-for-endpoint

# Policy Document for DMS
data "aws_iam_policy_document" "dms_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      identifiers = ["dms.amazonaws.com"]
      type        = "Service"
    }
  }
}

# Role for DMS
resource "aws_iam_role" "dms-vpc-role" {
  assume_role_policy = data.aws_iam_policy_document.dms_assume_role.json
  name               = "dms-vpc-role"
}

# Policy attachment for DMS role
resource "aws_iam_role_policy_attachment" "dms-vpc-role-AmazonDMSVPCManagementRole" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonDMSVPCManagementRole"
  role       = aws_iam_role.dms-vpc-role.name
}

# DMS access role to endpoint
resource "aws_iam_role" "dms-cdc-test" {
  assume_role_policy = data.aws_iam_policy_document.dms_assume_role.json
  name               = "dms-cdc-test"
}

resource "aws_iam_role_policy_attachment" "dms-access-for-endpoint-AmazonS3FullAccess" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
  role       = aws_iam_role.dms-cdc-test.name
}

# Cloudwatch Role
resource "aws_iam_role" "dms-cloudwatch-logs-role" {
  assume_role_policy = data.aws_iam_policy_document.dms_assume_role.json
  name               = "dms-cloudwatch-logs-role"
}

# Policy attachment for cloudwatch role
resource "aws_iam_role_policy_attachment" "dms-cloudwatch-logs-role-AmazonDMSCloudWatchLogsRole" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonDMSCloudWatchLogsRole"
  role       = aws_iam_role.dms-cloudwatch-logs-role.name
}