resource "aws_cloudwatch_log_metric_filter" "event_log_metric_filter_running" {
    name = "EventLogMetricFilterRunning"
    log_group_name = var.eventlog_group_name

    pattern = "[date, time, Instance, Service, status=Running]"

    metric_transformation {
        name = "IsRunning"
        namespace = "CustomServiceMetrics"
        value = 1
    dimensions = {
        Instance = "$Instance"
        Service  = "$Service"
    }
    
    }

}

resource "aws_cloudwatch_log_metric_filter" "event_log_metric_filter_not_running" {
    name = "EventLogMetricFilterNotRunning"
    log_group_name = var.eventlog_group_name

    pattern = "[date, time, Instance, Service, status!=Running]"

    metric_transformation {
        name = "NotRunning"
        namespace = "CustomServiceMetrics"
        value = 0
    dimensions = {
        Instance = "$Instance"
        Service  = "$Service"
    }
    
    }

}


resource "aws_cloudwatch_log_metric_filter" "spooler_log_metric_filter_running" {
    name = "SpoolerLogMetricFilterRunning"
    log_group_name = var.spoolerlog_group_name

    pattern = "[date, time, Instance, Service, status=Running]"

    metric_transformation {
        name = "IsRunning"
        namespace = "CustomServiceMetrics"
        value = 1
    dimensions = {
        Instance = "$Instance"
        Service  = "$Service"
    }
    
    }

}

resource "aws_cloudwatch_log_metric_filter" "spooler_log_metric_filter_not_running" {
    name = "SpoolerLogMetricFilterNotRunning"
    log_group_name = var.spoolerlog_group_name

    pattern = "[date, time, Instance, Service, status!=Running]"

    metric_transformation {
        name = "NotRunning"
        namespace = "CustomServiceMetrics"
        value = 0
    dimensions = {
        Instance = "$Instance"
        Service  = "$Service"
    }
    
    }

}
