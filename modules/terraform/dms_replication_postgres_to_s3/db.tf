# Test Postgres Instance
resource "aws_db_instance" "snowflake-replication-test" {
  vpc_security_group_ids = [aws_security_group.postgres-replication-test-sg.id]
  allocated_storage    = 50
  max_allocated_storage = 100
  db_name              = "testdb"
  identifier = "test-posgtres-snowflake-db"
  engine               = "postgres"
  engine_version       = "13.6"
  instance_class       = "db.t3.micro"
  username             = data.aws_ssm_parameter.rds_username.value
  password             = data.aws_ssm_parameter.rds_password.value
  parameter_group_name = aws_db_parameter_group.snowflake_replication.name
  skip_final_snapshot  = true
  ca_cert_identifier = "rds-ca-2019"
  publicly_accessible = true
  backup_retention_period = 3
  lifecycle {
    ignore_changes = [snapshot_identifier]
  }
}

output "endpoint" {
  value = trimsuffix(aws_db_instance.snowflake-replication-test.endpoint,":5432")
}


# Parameter Group Settings for logical replication
resource "aws_db_parameter_group" "snowflake_replication" {
  name   = "snowflake-replication-pg"
  family = "postgres13"
  description = "Parameter Group to enable logical replication for snowflake"

  parameter {
    name  = "rds.logical_replication"
    value = 1
    apply_method = "pending-reboot"
  }
  parameter {
    name = "max_wal_senders"
    value = 25
    apply_method = "pending-reboot"
  }
  parameter {
    name = "rds.log_retention_period"
    value = 2880
  }
  parameter {
    name = "wal_sender_timeout"
    value = 0
  }
  parameter {
    name = "max_slot_wal_keep_size"
    value = "50GB"
  }
}

# # Parameter Group Settings for logical replication on aurora
# resource "aws_rds_cluster_parameter_group" "default" {
#   name        = "rds-cluster-pg"
#   family      = "aurora5.6"
#   description = "RDS default cluster parameter group"
# }