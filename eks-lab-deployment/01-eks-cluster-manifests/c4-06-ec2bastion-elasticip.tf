# Create Elastic IP for Bastion Host
# Resource - depends_on Meta-Argument
resource "aws_eip" "bastion_eip" {
  depends_on = [module.ec2_public, data.terraform_remote_state.eks-vpc]
  count      = length(module.ec2_public) > 0 ? 1 : 0 # Create EIP only if there's at least one instance
  instance   = module.ec2_public[0].id
  domain     = "vpc"
  tags       = local.common_tags
}
