# Remote State Stack

This stack is for setting up and managing s3 backends for Terraform. It relies on workspaces to differentiate between different environments.

A makefile is provided to help with planning and applying to different environments. Run `make help` for more information.

## First Time Setup

* Initialize stack and create required environment(s)

```bash
make init
make new env=dev
```

* Create/Update the required .tfvars file(s)

```bash
touch dev.tfvars
```

* Define the following variables in that file. (Review the variables.tf file for more information on the different variables that can be defined.)

```text
customer                                     = "customer"
application                                  = "application"
region                                       = "us-east-1"
environment                                  = "dev"
abort_incomplete_multipart_upload_days       = 10
tfstate_noncurrent_version_expiration_days   = 30
tfstate_access_logs_expiration_days          = 90
tfstate_access_logs_transition_storage_class = "STANDARD_IA"
tfstate_access_logs_transition_days          = 30

```

## Subsequent Plans/Applies

```bash
make plan env=dev
make apply env=dev
```

Note: Be sure to track the resulting state file(s) in Git so other developers can make changes to the backend as necessary.
