variable "region" {
  type        = string
  description = "The region in which to deploy the remote backend resources."
}
variable "customer" {
  type        = string
  description = "A unique identifier to differentiate this deployment."
}
variable "application" {
  type        = string
  description = "A unique identifier to differentiate this deployment."
}
variable "environment" {
  type        = string
  description = "Environment name, such as 'dev', 'test', or 'prod'"
}
# variable "rds_password" {
#   type = string
#   description = "Password for rds user"
#   sensitive = true
# }
variable "vpc_id" {
  type = string
  description = "VPC for porstgres replication"
}
# variable "rds_username" {
#   type = string
#   description = "RDS Username"
#   sensitive = true
# }