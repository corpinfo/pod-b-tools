*Acceptance Criteria*
* Reviewed Public IP Assignements on private subnets by default
* Reviewed NAT configuration
* Reviewed NAT HA
* Suggested changes to better protect the network in this task

