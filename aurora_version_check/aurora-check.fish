#!/usr/bin/env fish

# Extract all profile names from the credentials file
set profiles (grep -E '^\[.*\]' ~/.aws/credentials | tr -d '[]')

# Loop through all profiles
for profile in $profiles
  # Define the regions to include
  set regions (aws ec2 describe-regions --region us-east-1 --profile $profile | jq -r '.Regions[] | .RegionName')
  # Loop through all regions
  for region in $regions
    echo "Profile: $profile, Region: $region"
    # Run the command for the profile and region
    set output (aws rds describe-db-clusters --include-share --query 'DBClusters[?Engine==`aurora`].{EV:EngineVersion, DBCI:DBClusterIdentifier, EM:EngineMode}' --output table --profile "$profile" --region "$region" 2>&1)
    if test $status -ne 0
      echo "Error: $output"
      echo "Profile: $profile"
      echo "Region: $region"
    end
  end
end