<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.34.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_dynamodb_table.terraform_state_lock_table](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table) | resource |
| [aws_iam_user.pipeline_user](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user) | resource |
| [aws_iam_user_policy_attachment.pipeline_user_attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user_policy_attachment) | resource |
| [aws_s3_bucket.logs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket.tfstate](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_acl.logs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_acl) | resource |
| [aws_s3_bucket_acl.tfstate](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_acl) | resource |
| [aws_s3_bucket_lifecycle_configuration.logs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_lifecycle_configuration) | resource |
| [aws_s3_bucket_lifecycle_configuration.tfstate](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_lifecycle_configuration) | resource |
| [aws_s3_bucket_logging.tfstate](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_logging) | resource |
| [aws_s3_bucket_public_access_block.logs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_s3_bucket_public_access_block.tfstate](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_s3_bucket_server_side_encryption_configuration.logs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_server_side_encryption_configuration) | resource |
| [aws_s3_bucket_server_side_encryption_configuration.tfstate](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_server_side_encryption_configuration) | resource |
| [aws_s3_bucket_versioning.logs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_versioning) | resource |
| [aws_s3_bucket_versioning.tfstate](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_versioning) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_abort_incomplete_multipart_upload_days"></a> [abort\_incomplete\_multipart\_upload\_days](#input\_abort\_incomplete\_multipart\_upload\_days) | The number of days to wait before deleting incomplete multipart uploads. | `number` | `10` | no |
| <a name="input_application"></a> [application](#input\_application) | A unique identifier to differentiate this deployment. | `string` | n/a | yes |
| <a name="input_customer"></a> [customer](#input\_customer) | A unique identifier to differentiate this deployment. | `string` | n/a | yes |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment name, such as 'dev', 'test', or 'prod' | `string` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | The region in which to deploy the remote backend resources. | `string` | n/a | yes |
| <a name="input_tfstate_access_logs_expiration_days"></a> [tfstate\_access\_logs\_expiration\_days](#input\_tfstate\_access\_logs\_expiration\_days) | The number of days to wait before deleting tfstate access logs.<br>This number must be greater than var.tfstate\_access\_logs\_transition\_days | `number` | n/a | yes |
| <a name="input_tfstate_access_logs_transition_days"></a> [tfstate\_access\_logs\_transition\_days](#input\_tfstate\_access\_logs\_transition\_days) | The number of days to wait before moving access logs to the storage class<br>defined by var.tfstate\_access\_logs\_transition\_storage\_class. This number<br>must be less than var.tfstate\_access\_logs\_expiration\_days. This number<br>must also adhere to the constraints documented here:<br>https://docs.aws.amazon.com/AmazonS3/latest/userguide/lifecycle-transition-general-considerations.html<br>which are dependent on which storage class is passed in for<br>var.tfstate\_access\_logs\_transition\_storage\_class | `number` | n/a | yes |
| <a name="input_tfstate_access_logs_transition_storage_class"></a> [tfstate\_access\_logs\_transition\_storage\_class](#input\_tfstate\_access\_logs\_transition\_storage\_class) | The storage class to which access logs should be moved to. | `string` | n/a | yes |
| <a name="input_tfstate_noncurrent_version_expiration_days"></a> [tfstate\_noncurrent\_version\_expiration\_days](#input\_tfstate\_noncurrent\_version\_expiration\_days) | The number of days to wait before deleting noncurrent objects from the tfstate bucket. | `number` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_dynamo_db_table_name"></a> [dynamo\_db\_table\_name](#output\_dynamo\_db\_table\_name) | n/a |
| <a name="output_s3_bucket_name"></a> [s3\_bucket\_name](#output\_s3\_bucket\_name) | n/a |
<!-- END_TF_DOCS -->