(Get-SSMParameter -Name windows_log_rotation).Value | Out-File (New-Item -Path "C:\temp\logrotate.ps1" -Force) -Encoding utf8 

$task="LogRotate"
Get-ScheduledTask -TaskName $task -ErrorAction SilentlyContinue -OutVariable taskStatus1
if ($taskStatus1) { Unregister-ScheduledTask -TaskName $task -Confirm:$false } 


$action = New-ScheduledTaskAction -Execute 'Powershell.exe' -Argument '-NoProfile -WindowStyle Hidden -file "C:\temp\logrotate.ps1" ' 
$trigger = New-ScheduledTaskTrigger -Once -At (Get-Date).AddMinutes(5) -RepetitionInterval (New-TimeSpan -Hours 2) 
$principal = New-ScheduledTaskPrincipal -UserId "NT AUTHORITY\SYSTEM" -LogonType ServiceAccount -RunLevel Highest 
$settings = New-ScheduledTaskSettingsSet -Compatibility At -ExecutionTimeLimit (New-TimeSpan -Minutes 10) 

Register-ScheduledTask -TaskName $task -Action $action -Principal $principal -Trigger $trigger -Settings $settings